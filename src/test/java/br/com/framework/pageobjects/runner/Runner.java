package br.com.framework.pageobjects.runner;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.framework.pageobjects.driverfactory.DriverFactory;
import br.com.framework.pageobjects.enumerator.SelectBrowser;
import br.com.framework.pageobjects.tests.ValidarConsultasEmPrecosPrazos;


@RunWith(Suite.class)
@SuiteClasses({
	ValidarConsultasEmPrecosPrazos.class
})

public class Runner {

	@BeforeClass
	public static void start() throws IOException {
		DriverFactory.createDriver(SelectBrowser.CHROME);
		System.out.println("Inicializando driver...");
	}
	
	@AfterClass
	public static void finish() {
		System.out.println("Fechando driver....");
		DriverFactory.quitDriver();
	}
}
