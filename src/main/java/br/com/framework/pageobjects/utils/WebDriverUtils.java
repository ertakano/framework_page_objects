package br.com.framework.pageobjects.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.framework.pageobjects.driverfactory.DriverFactory;

public class WebDriverUtils {

	private static WebDriver driver = DriverFactory.getDriver();

	private static long timeout = 10;
	private static WebDriverWait wait;
	
	public WebDriverUtils() {
		wait = new WebDriverWait(driver, timeout);
	}
	
	public static void awaitsElementToGoAway(By by) {
		
		wait.until(ExpectedConditions.invisibilityOf(driver.findElement(by)));
	}
	
	public static void awaitsElementToGoAppear(By by) {

		WebElement element = driver.findElement(by);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public static void awaitsElementToGoAppearByName(String name) {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
	}
	
	public static void awaitsElementToGoAppearById(String id) {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
	}
	
	public static void awaitsElementToGoAppearByXpath(String xpath) {
		
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS); 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}
	
	public static void switchToFrame (String idOrName) {
		driver.switchTo().frame(idOrName);
	}
	
	public static void switchToDefaultContent () {
		driver.switchTo().defaultContent();
	}
	
	public String getTextByXpath(String xpath) {
		return driver.findElement(By.xpath(xpath)).getText();
	}
	
	protected boolean containsElement(By by) throws Exception {
		try {
			WebDriverUtils.awaitsElementToGoAppear(by);
			if (!driver.findElement(by).equals(null)) {
				return true;
			}
		} catch (Exception e) {
			throw new Exception("O elemento não foi encontrado", e);
		}
		return false;
	}
	
	protected boolean containsElementsByText(String text) throws Exception {
		try {
			if (driver.getPageSource().contains(text)) {
				return true;
			}
		} catch (Exception e) {
			throw new Exception("O texto: " + text + " não foi encontrado", e);
		}
		return false;
	}
		
	protected boolean containsTextInElement(By by, String text) throws Exception {
		try {
			WebDriverUtils.awaitsElementToGoAppear(by);
			if (driver.findElement(by).getText().equals(text)) {
				return true;
			}
		} catch (Exception e) {
			throw new Exception("O texto: " + text + "não foi encontrado", e);
		}
		return false;
	}

	protected boolean isEnabled(By by) {
		WebDriverUtils.awaitsElementToGoAppear(by);
		return driver.findElement(by).isEnabled();
	}
	
	protected void click(By by) {
		WebDriverUtils.awaitsElementToGoAppear(by);
		driver.findElement(by).click();
	}

	protected void clear(By by) {
		WebDriverUtils.awaitsElementToGoAppear(by);
		driver.findElement(by).clear();
	}
	
	protected void writeText(By by, String text) {
		clear(by);
		driver.findElement(by).sendKeys(text);
	}
	
	protected void selectElementListBox(By by, String itemValue) {
		WebDriverUtils.awaitsElementToGoAppear(by);
		new Select(driver.findElement(by)).selectByVisibleText(itemValue);
	}

	protected void navigateTo(String url) {
		driver.navigate().to(url);
	}
		
	public void alterTab() {
		for(String winHandle : driver.getWindowHandles()){
			driver.switchTo().window(winHandle);
			}
	}
	
	public void returnAlterTab() {
		for(String winHandleBefore : driver.getWindowHandles()){
			driver.switchTo().window(winHandleBefore);
		}
	}
	
	public String getText(By by) {
		return driver.findElement(by).getText();
	}
	
}

